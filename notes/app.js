const express = require('express');
const app = express();
// var mongo = require('mongoskin');
// var db = mongo.db('mongodb://localhost:27017/notes', {native_parser: true});
const mongo = require('mongodb');

let db
mongo.MongoClient().connect('mongodb://localhost:27017/notes')
    .then(_db => {
        db = _db;
    })

const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));

const formatNotes = function (notes) {
    if (!notes) {
        return 'Error';
    }

    let ret = '';
    for (let i = 1; i <= notes.length; i++) {
        const note = notes[i - 1];
        ret += i + '\t';
        ret += note.note;
        ret += '\n';
    }
    return ret;
};

const password = 'yonilyonilyonilpaassword';
app.use(function(req, res, next) {
    const query = req.query || null;
    if (query && query.pw == password && query.username) {
        req.username = query.username;
        next();
    } else {
        res.send('Incorrect password!');
        res.end();
    }
});

app.route('/get').get(function(req, res, next) {
    let cursor = db.collection('notes').find({
        username: req.username
    }).sort({timestamp: -1});
    let limit = parseInt(req.query.num);
    if (!isNaN(limit) && typeof limit === 'number') {
        cursor = cursor.limit(limit);
    }

    cursor.toArray(function(err, items) {
        res.send(formatNotes(items));
    });
});

app.route('/getall').get(function(req, res, next) {
    db.collection('notes').find({
        username: req.username
    }).sort({timestamp: -1}).toArray(function(err, items) {
        res.send(formatNotes(items));
    });
});

app.route('/add').post(function(req, res, next) {
    db.collection('notes').insert({
        note: req.body.note,
        timestamp: Date.now(),
        username: req.username
    }, function(err, result) {
        if (err) {
            console.log(err);
        }
        res.send();
    });
});


app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    console.log(err);
    res.end()
});

module.exports = app;
